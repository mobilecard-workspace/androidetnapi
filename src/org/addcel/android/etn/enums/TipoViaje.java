package org.addcel.android.etn.enums;

public enum TipoViaje {
	
	SENCILLO("S"), REDONDO("R");
	
	private String clave;
	
	private TipoViaje(String _clave) {
		clave = _clave;
	}
	
	public String getClave() {
		return clave;
	}
}
