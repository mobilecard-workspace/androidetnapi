package org.addcel.android.etn.to;

import org.addcel.android.interfaces.JSONable;
import org.addcel.android.interfaces.Mapable;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class DestinoReq implements Mapable, JSONable {
	
	BasicNameValuePair origen;
	
	public DestinoReq() {
		
	}
	
	public DestinoReq setOrigen(BasicNameValuePair _origen) {
		origen = _origen;
		
		return this;
	}

	@Override
	public BasicNameValuePair[] toMap() {
		// TODO Auto-generated method stub
		
		BasicNameValuePair[] map = new BasicNameValuePair[1];
		map[0] = new BasicNameValuePair("origen", origen.getValue());
		
		return map;
	}

	@Override
	public JSONObject toJSON() {
		// TODO Auto-generated method stub
		try {
			return new JSONObject().put("origen", origen.getValue());
		} catch (JSONException e) {
			return null;
		}
	}

	@Override
	public void fromJSON(JSONObject json) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return toJSON().toString();
	}

}
