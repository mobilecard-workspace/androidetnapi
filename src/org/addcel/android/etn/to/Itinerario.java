package org.addcel.android.etn.to;

import org.addcel.android.interfaces.JSONable;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class Itinerario implements JSONable, Parcelable {

	private String oficina;
	private String horaSalida;
	private String fechaSalida;

	public Itinerario() {

	}
	
	public Itinerario(Parcel source) {
		
	}
	
	public Itinerario(JSONObject json) {
		fromJSON(json);
	}

	public String getOficina() {
		return oficina;
	}

	public String getHoraSalida() {
		return horaSalida;
	}

	public String getFechaSalida() {
		return fechaSalida;
	}

	public Itinerario setOficina(String oficina) {
		this.oficina = oficina;
		
		return this;
	}

	public Itinerario setHoraSalida(String horaSalida) {
		this.horaSalida = horaSalida;
		
		return this;
	}

	public Itinerario setFechaSalida(String fechaSalida) {
		this.fechaSalida = fechaSalida;
		
		return this;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(oficina);
		dest.writeString(horaSalida);
		dest.writeString(fechaSalida);
	}
	
	public void readFromParcel(Parcel source) {
		oficina = source.readString();
		horaSalida = source.readString();
		fechaSalida = source.readString();
	}

	@Override
	public JSONObject toJSON() {
		// TODO Auto-generated method stub
		try {
			return new JSONObject().put("oficina", oficina)
									.put("horaSalida", horaSalida)
									.put("fechaSalida", fechaSalida);
		} catch (JSONException e) {
			return null;	
		}
	}

	@Override
	public void fromJSON(JSONObject json) {
		// TODO Auto-generated method stub
		oficina = json.optString("oficina");
		horaSalida = json.optString("horaSalida");
		fechaSalida = json.optString("fechaSalida");
	}
	
	public static final Parcelable.Creator<Itinerario> CREATOR = new Creator<Itinerario>() {
		
		@Override
		public Itinerario[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Itinerario[size];
		}
		
		@Override
		public Itinerario createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new Itinerario(source);
		}
	};
	
	public String toString() {
		
		return "Oficina: " + oficina + 
				"\nFecha de salida: " + fechaSalida +
				"\nHora de salida: " + horaSalida + "\n";
	};

}
