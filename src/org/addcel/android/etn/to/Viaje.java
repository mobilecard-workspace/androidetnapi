package org.addcel.android.etn.to;

import java.util.ArrayList;
import java.util.List;

import org.addcel.android.etn.enums.TipoViaje;
import org.addcel.android.interfaces.JSONable;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class Viaje implements JSONable, Parcelable {

    private String idOrigen;
    private String idDestino;
    private String origen;
    private String destino;
    private int numAdulto;
    private int numNino;
    private int numInsen;
    private int numEstudiante;
    private int numMaestro;
    private Corrida corrida;
    private List<Asiento> asientos;
    private TipoViaje tipoViaje;
    private double tarifaAdulto;
    private double tarifaNino;
    private double tarifaInsen;
    private double tarifaEstudiante;
    private double tarifaMaestro;
    private double subTotal; // SE SUMAN TARIFAS CORRESPONDIENTES A CADA OBJETO EN LA LISTA "asientos"
    
    public Viaje() {
    	asientos = new ArrayList<Asiento>();
    }
    
    public Viaje(Parcel source) {
    	this();
    	readFromParcel(source);
    }
    
    public Viaje(JSONObject json) {
    	this();
    	fromJSON(json);
    }
    
    public String getIdOrigen() {
		return idOrigen;
	}

	public String getIdDestino() {
		return idDestino;
	}

	public String getOrigen() {
		return origen;
	}

	public String getDestino() {
		return destino;
	}

	public int getNumAdulto() {
		return numAdulto;
	}

	public int getNumNino() {
		return numNino;
	}

	public int getNumInsen() {
		return numInsen;
	}

	public int getNumEstudiante() {
		return numEstudiante;
	}

	public int getNumMaestro() {
		return numMaestro;
	}

	public Corrida getCorrida() {
		return corrida;
	}

	public List<Asiento> getAsientos() {
		return asientos;
	}

	public TipoViaje getTipoViaje() {
		return tipoViaje;
	}

	public double getTarifaAdulto() {
		return tarifaAdulto;
	}

	public double getTarifaNino() {
		return tarifaNino;
	}

	public double getTarifaInsen() {
		return tarifaInsen;
	}

	public double getTarifaEstudiante() {
		return tarifaEstudiante;
	}

	public double getTarifaMaestro() {
		return tarifaMaestro;
	}
	
	public double getSubTotal() {
		return subTotal;
	}

	public Viaje setIdOrigen(String idOrigen) {
		this.idOrigen = idOrigen;
		
		return this;
	}

	public Viaje setIdDestino(String idDestino) {
		this.idDestino = idDestino;
		return this;
	}

	public Viaje setOrigen(String origen) {
		this.origen = origen;
		return this;
	}

	public Viaje setDestino(String destino) {
		this.destino = destino;
		return this;
	}

	public Viaje setNumAdulto(int numAdulto) {
		this.numAdulto = numAdulto;
		return this;
	}

	public Viaje setNumNino(int numNino) {
		this.numNino = numNino;
		return this;
	}

	public Viaje setNumInsen(int numInsen) {
		this.numInsen = numInsen;
		return this;
	}

	public Viaje setNumEstudiante(int numEstudiante) {
		this.numEstudiante = numEstudiante;
		return this;
	}

	public Viaje setNumMaestro(int numMaestro) {
		this.numMaestro = numMaestro;
		return this;
	}

	public Viaje setCorrida(Corrida corrida) {
		this.corrida = corrida;
		return this;
	}

	public Viaje setAsientos(List<Asiento> asientos) {
		this.asientos = asientos;
		return this;
	}

	public Viaje setTipoViaje(TipoViaje tipoViaje) {
		this.tipoViaje = tipoViaje;
		return this;
	}

	public Viaje setTarifaAdulto(double tarifaAdulto) {
		this.tarifaAdulto = tarifaAdulto;
		return this;
	}

	public Viaje setTarifaNino(double tarifaNino) {
		this.tarifaNino = tarifaNino;
		return this;
	}

	public Viaje setTarifaInsen(double tarifaInsen) {
		this.tarifaInsen = tarifaInsen;
		return this;
	}

	public Viaje setTarifaEstudiante(double tarifaEstudiante) {
		this.tarifaEstudiante = tarifaEstudiante;
		return this;
	}

	public Viaje setTarifaMaestro(double tarifaMaestro) {
		this.tarifaMaestro = tarifaMaestro;
		
		return this;
	}
	
	private double calcularSubtotal() {
		for (Asiento a : asientos) {
			switch (a.getPasajero()) {
			case ADULTO:
				subTotal += getTarifaAdulto();
				break;
			case NINO:
				subTotal += getTarifaNino();
				break;
			case INSEN:
				subTotal += getTarifaInsen();
				break;
			case ESTUDIANTE:
				subTotal += getTarifaEstudiante();
				break;
			case MAESTRO:
				subTotal += getTarifaMaestro();
				break;
			default:
				break;
			}
		}
		
		return subTotal;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
	    dest.writeString(idOrigen);
	    dest.writeString(idDestino);
	    dest.writeString(origen);
	    dest.writeString(destino);
	    dest.writeInt(numAdulto);
	    dest.writeInt(numNino);
	    dest.writeInt(numInsen);
	    dest.writeInt(numEstudiante);
	    dest.writeInt(numMaestro);
	    dest.writeParcelable(corrida, flags);
	    dest.writeTypedList(asientos);
	    dest.writeSerializable(tipoViaje);
	    dest.writeDouble(tarifaAdulto);
	    dest.writeDouble(tarifaNino);
	    dest.writeDouble(tarifaInsen);
	    dest.writeDouble(tarifaEstudiante);
	    dest.writeDouble(tarifaMaestro);
	    dest.writeDouble(calcularSubtotal());
	}
	
	public void readFromParcel(Parcel source) {
		idOrigen = source.readString();
		idDestino = source.readString();
		origen = source.readString();
		destino = source.readString();
		numAdulto = source.readInt();
		numNino = source.readInt();
		numInsen = source.readInt();
		numEstudiante = source.readInt();
		numMaestro = source.readInt();
		corrida = source.readParcelable(Corrida.class.getClassLoader());
		source.readTypedList(asientos, Asiento.CREATOR);
		tipoViaje = (TipoViaje) source.readSerializable();
		tarifaAdulto = source.readDouble();
		tarifaNino = source.readDouble();
		tarifaInsen = source.readDouble();
		tarifaEstudiante = source.readDouble();
		tarifaMaestro = source.readDouble();
		subTotal = source.readDouble();
	}
	
	public static final Parcelable.Creator<Viaje> CREATOR = new Creator<Viaje>() {
		
		@Override
		public Viaje[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Viaje[size];
		}
		
		@Override
		public Viaje createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new Viaje(source);
		}
	};

	@Override
	public JSONObject toJSON() {
		
		try {
			return new JSONObject().put( "idOrigen", idOrigen)
									.put("idDestino", idDestino)
									.put("origen", origen)
									.put("destino", destino)
									.put("fecha", corrida.getFechaInicial())
									.put("hora", corrida.getHoraCorrida())
									.put("numAdulto",numAdulto)
									.put("numNino", numNino)
									.put("numInsen", numInsen)
									.put("numEstudiante", numEstudiante)
									.put("numMaestro", numMaestro)
									.put("corrida", corrida.getCorrida())
									.put("tipoCorrida", corrida.getTipoCorrida())
									.put("asientos", formatAsientos(asientos))
									.put("nombres", formatNombres(asientos))
									.put("tipoProducto", "ETN")
									.put("tipoViaje", tipoViaje.getClave())
									.put("tarifaAdulto", tarifaAdulto)
									.put("tarifaNino", tarifaNino)
									.put("tarifaInsen", tarifaInsen)
									.put("tarifaEstudiante", tarifaEstudiante)
									.put("tarifaMaestro", tarifaMaestro);
		} catch (JSONException e) {

			return null;
		}
	}

	@Override
	public void fromJSON(JSONObject json) {
		// TODO Auto-generated method stub

	}
	
	private String formatAsientos(List<Asiento> asientos) {
		
		String asientosString = "";
		
		int len = asientos.size();
		
		for (int i = 0; i < len; i++) {
			
			asientosString += asientos.get(i).getNumAsiento();
			
			if ((len - 1) >  i)
				asientosString += "|";
		}
		
		return asientosString;
		
	}
	
	private String formatNombres(List<Asiento> asientos) {
		
		String nombresString = "";
		
		int len = asientos.size();
		
		for (int i = 0; i < len; i++) {
			nombresString += asientos.get(i).getNombre();
			
			if ((len -1) > i)
				nombresString += ",";
		}
		
		return nombresString;
	}

}
