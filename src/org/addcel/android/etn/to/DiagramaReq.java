package org.addcel.android.etn.to;

import java.util.Date;

import org.addcel.android.interfaces.JSONable;
import org.addcel.android.interfaces.Mapable;
import org.addcel.android.util.Text;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class DiagramaReq implements Mapable, JSONable {
	
	private Corrida corrida;
	
	public DiagramaReq(Corrida c) {
		corrida = c;
	}
	
	private String getDateWithFormat() {
		
		Log.i("DiagramaReq", "Fecha sin formato" + corrida.getFechaInicial());
		
		Date d = Text.getEtnDateFromStringWithSlash(corrida.getFechaInicial());
		return Text.formatDateNoSpace(d);
		
	}

	@Override
	public BasicNameValuePair[] toMap() {
		// TODO Auto-generated method stub
		
		BasicNameValuePair[] map = new  BasicNameValuePair[2];
		map[0] = new BasicNameValuePair("fecha", getDateWithFormat());
		map[1] = new BasicNameValuePair("corrida", corrida.getCorrida());

		return map;
	}

	@Override
	public JSONObject toJSON() {
		// TODO Auto-generated method stub
		
		try {
			return new JSONObject()
			.put("fecha", getDateWithFormat())
			.put("corrida", corrida.getCorrida());
		} catch (JSONException e) {
			return null;
		}
	}

	@Override
	public void fromJSON(JSONObject json) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return toJSON().toString();
	}

}
