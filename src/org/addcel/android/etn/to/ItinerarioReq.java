package org.addcel.android.etn.to;

import java.util.Date;

import org.addcel.android.interfaces.JSONable;
import org.addcel.android.interfaces.Mapable;
import org.addcel.android.util.Text;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class ItinerarioReq implements JSONable, Mapable {

	private String origen;
	private Corrida corrida;

	public ItinerarioReq() {
	}

	public String getOrigen() {
		return origen;
	}

	public Corrida getCorrida() {
		return corrida;
	}

	public ItinerarioReq setOrigen(String origen) {
		this.origen = origen;
		
		return this;
	}

	public ItinerarioReq setCorrida(Corrida corrida) {
		this.corrida = corrida;
		
		return this;
	}

	private String getDateWithFormat() {

		Log.i("DiagramaReq", "Fecha sin formato" + corrida.getFechaInicial());
		Date d = Text.getEtnDateFromStringWithSlash(corrida.getFechaInicial());
		return Text.formatDateNoSpace(d);

	}

	@Override
	public BasicNameValuePair[] toMap() {
		// TODO Auto-generated method stub

		BasicNameValuePair[] map = new BasicNameValuePair[3];
		map[0] = new BasicNameValuePair("origen", origen);
		map[1] = new BasicNameValuePair("fecha", getDateWithFormat());
		map[2] = new BasicNameValuePair("corrida", corrida.getCorrida());

		return map;
	}

	@Override
	public JSONObject toJSON() {
		// TODO Auto-generated method stub
		
		try {
			
			return new JSONObject()
				.put("origen", origen)
				.put("fecha", getDateWithFormat())
				.put("corrida", corrida.getCorrida());
			
		} catch (JSONException e) {

			return null;
		}
		
	}

	@Override
	public void fromJSON(JSONObject json) {
		// TODO Auto-generated method stub

	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return toJSON().toString();
	}
}
