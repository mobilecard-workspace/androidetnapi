package org.addcel.android.etn.to;

import java.util.ArrayList;
import java.util.List;

import org.addcel.android.etn.enums.TipoPasajero;
import org.addcel.android.interfaces.JSONable;
import org.addcel.android.interfaces.Mapable;
import org.addcel.android.util.DeviceUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class ReservacionReq implements JSONable, Mapable {

	private long idUsuario;
	private int idProveedor;
	private String mail;
	private double total;
	private List<Viaje> viajes;
	private String password;
	private String token;
	private Context con;

	public ReservacionReq(Context _con) {
		con = _con;
		viajes = new ArrayList<Viaje>();
	}

	public long getIdUsuario() {
		return idUsuario;
	}

	public int getIdProveedor() {
		return idProveedor;
	}

	public String getMail() {
		return mail;
	}

	public double getTotal() {
		return total;
	}

	public List<Viaje> getViajes() {
		return viajes;
	}

	public String getPassword() {
		return password;
	}

	public String getToken() {
		return token;
	}

	public ReservacionReq setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;

		return this;
	}

	public ReservacionReq setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;

		return this;
	}

	public ReservacionReq setMail(String mail) {
		this.mail = mail;

		return this;
	}

	public ReservacionReq setTotal(double total) {
		this.total = total;

		return this;
	}

	public ReservacionReq setViajes(List<Viaje> viajes) {
		this.viajes = viajes;

		return this;
	}

	public ReservacionReq setPassword(String password) {
		this.password = password;

		return this;
	}

	public ReservacionReq setToken(String token) {
		this.token = token;

		return this;
	}
	
	private double calcularTotal() {
		
		double sum = 0;
		
		
		for (Viaje v : viajes) {
			total = v.getSubTotal();
		}
		
		return sum;
		
	}

	@Override
	public JSONObject toJSON() {
		// TODO Auto-generated method stub
		try {
			
			JSONObject json = new JSONObject()
				.put("idUsuario", idUsuario)
			    .put("idProveedor", idProveedor) // EN EL CASO DE ONCOMERCIO SER�A EL #26.
			    .put("mail", mail)
			    .put("total", calcularTotal())
				.put("password", password)
			    .put("token", token)
			    .put("imei", DeviceUtils.getIMEI(con))
			    .put("modelo", DeviceUtils.getModel())
			    .put("software", DeviceUtils.getSWVersion());
			
			JSONArray array = new JSONArray();
			
			for (Viaje v : viajes)
				array.put(v.toJSON());
			
			json.put("viajes", array);
		   
			return json;
			
		} catch (JSONException e) {
			return null;
		}
	}

	@Override
	public void fromJSON(JSONObject json) {
		// TODO Auto-generated method stub

	}

	@Override
	public BasicNameValuePair[] toMap() {
		// TODO Auto-generated method stub

		BasicNameValuePair[] map = new BasicNameValuePair[29];
		return map;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return toJSON().toString();
	}

}
