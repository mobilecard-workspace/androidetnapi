package org.addcel.android.etn.to;

import org.addcel.android.etn.enums.TipoPasajero;

import android.os.Parcel;
import android.os.Parcelable;

public class Asiento implements Parcelable {

	private String numAsiento;
	private int ocupado;
	private boolean esAsiento;
	private String nombre;
	private TipoPasajero pasajero;
	
	public Asiento() {}
	
	public Asiento(Parcel source) {
		readFromParcel(source);
	}

	public String getNumAsiento() {
		return numAsiento;
	}

	public int getOcupado() {
		return ocupado;
	}

	public boolean isEsAsiento() {
		return esAsiento;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public TipoPasajero getPasajero() {
		return pasajero;
	}

	public Asiento setNumAsiento(String numAsiento) {
		this.numAsiento = numAsiento;
		
		return this;
	}

	public Asiento setOcupado(int ocupado) {
		this.ocupado = ocupado;
		
		return this;
	}

	public Asiento setEsAsiento(boolean esAsiento) {
		this.esAsiento = esAsiento;
		
		return this;
	}
	
	public Asiento setNombre(String nombre) {
		this.nombre = nombre;
		
		return this;
	}
	
	public Asiento setPasajero(TipoPasajero pasajero) {
		this.pasajero = pasajero;
		
		return this;
	}

	@Override
	public String toString() {
		return "Asiento [numAsiento=" + numAsiento + ", ocupado=" + ocupado
				+ "]";
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(numAsiento);
		dest.writeInt(ocupado);
		dest.writeByte((byte) (esAsiento ? 1:0));
		dest.writeString(nombre);
		dest.writeSerializable(pasajero);
	}
	
	public void readFromParcel(Parcel source) {
		numAsiento = source.readString();
		ocupado = source.readInt();
		esAsiento = source.readByte() != 0;
		nombre = source.readString();
		pasajero = (TipoPasajero) source.readSerializable();
	}
	
	public static final Parcelable.Creator<Asiento> CREATOR = new Creator<Asiento>() {
		
		@Override
		public Asiento[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Asiento[size];
		}
		
		@Override
		public Asiento createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new Asiento(source);
		}
	};
	
	
}
