package org.addcel.android.etn.to;

import org.addcel.android.etn.enums.TipoViaje;

public class ProcessSingleton {

	private static ProcessSingleton instance = null;

	private TipoViaje tipo;

	private String origen;
	private String origenClave;
	private String destino;
	private String destinoClave;
	private int adultos;
	private int ninos;
	private int insen;
	private int maestros;
	private int estudiantes;
	private Corrida corridaSalida;
	private Corrida corridaRegreso;
	private Diagrama diagramaSalida;
	private Diagrama diagramaRegreso;
	private Viaje viajeSalida;
	private Viaje viajeRegreso;
	
	private ProcessSingleton() {
	}

	public static ProcessSingleton getInstance() {
		if (null == instance)
			instance = new ProcessSingleton();

		return instance;
	}

	public TipoViaje getTipoViaje() {
		return tipo;
	}

	public String getOrigen() {
		return origen;
	}

	public String getOrigenClave() {
		return origenClave;
	}

	public String getDestino() {
		return destino;
	}

	public String getDestinoClave() {
		return destinoClave;
	}

	public int getAdultos() {
		return adultos;
	}

	public int getNinos() {
		return ninos;
	}

	public int getInsen() {
		return insen;
	}

	public int getMaestros() {
		return maestros;
	}

	public int getEstudiantes() {
		return estudiantes;
	}

	public Corrida getCorridaSalida() {
		return corridaSalida;
	}

	public Corrida getCorridaRegreso() {
		return corridaRegreso;
	}
	
	public Diagrama getDiagramaSalida() {
		return diagramaSalida;
	}
	
	public Diagrama getDiagramaRegreso() {
		return diagramaRegreso;
	}
	
	public Viaje getViajeSalida() {
		return viajeSalida;
	}
	
	public Viaje getViajeRegreso() {
		return viajeRegreso;
	}

	public void setRedondo(TipoViaje tipoViaje) {
		this.tipo = tipoViaje;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public void setOrigenClave(String origenClave) {
		this.origenClave = origenClave;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public void setDestinoClave(String destinoClave) {
		this.destinoClave = destinoClave;
	}

	public void setAdultos(int adultos) {
		this.adultos = adultos;
	}

	public void setNinos(int ninos) {
		this.ninos = ninos;
	}

	public void setInsen(int insen) {
		this.insen = insen;
	}

	public void setMaestros(int maestros) {
		this.maestros = maestros;
	}

	public void setEstudiantes(int estudiantes) {
		this.estudiantes = estudiantes;
	}

	public void setCorridaSalida(Corrida corridaSalida) {
		this.corridaSalida = corridaSalida;
	}

	public void setCorridaRegreso(Corrida corridaRegreso) {
		this.corridaRegreso = corridaRegreso;
	}

	public void setDiagramaSalida(Diagrama diagramaSalida) {
		this.diagramaSalida = diagramaSalida;
	}

	public void setDiagramaRegreso(Diagrama diagramaRegreso) {
		this.diagramaRegreso = diagramaRegreso;
	}
	
	public void setViajeSalida(Viaje viajeSalida) {
		this.viajeSalida = viajeSalida;
	}
	
	public void setViajeRegreso(Viaje viajeRegreso) {
		this.viajeRegreso = viajeRegreso;
	}
	
	
}
