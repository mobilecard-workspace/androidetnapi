package org.addcel.android.etn.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.addcel.android.crypto.Cifrados;
import org.addcel.android.etn.R;
import org.addcel.android.etn.constant.Url;
import org.addcel.android.etn.enums.Catalogos;
import org.addcel.android.etn.enums.TipoViaje;
import org.addcel.android.etn.to.Corrida;
import org.addcel.android.etn.to.CorridaReq;
import org.addcel.android.etn.to.DestinoReq;
import org.addcel.android.etn.to.ProcessSingleton;
import org.addcel.android.util.ErrorUtil;
import org.addcel.android.widget.SpinnerValues;
import org.addcel.android.widget.SpinnerValuesListener;
import org.addcel.android.ws.client.WSClient;
import org.addcel.android.ws.listener.WSResponseListener;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;

public class FirstActivity extends SherlockActivity {
	
	private Spinner origenSpinner, destinoSpinner;
	private SpinnerValues origenValues, destinoValues;
	
	private List<BasicNameValuePair> origenes;
	private List<BasicNameValuePair> destinos;
	
	private int adultos;
	private int ninos;
	private int insen;
	private int maestros;
	private int estudiantes;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_first);
		
		origenSpinner = (Spinner) findViewById(R.id.spinner_origen);
		destinoSpinner = (Spinner) findViewById(R.id.spinner_destino);

		new WSClient(FirstActivity.this)
			.hasLoader(false)
			.setCancellable(false)
			.setCatalogo(Catalogos.ORIGEN)
			.setCifrado(Cifrados.HARD)
			.setListener(new OrigenListener())
			.setUrl(Url.ORIGEN_GET)
			.execute(new JSONObject().toString());

	}
	
	public void getCorridas(View v) {
		
		adultos = 1;
		ninos = 0;
		insen = 0;
		maestros = 0;
		estudiantes = 0;
		
		CorridaReq request = new CorridaReq().setDestino(destinoValues.getValorSeleccionado())
											.setFechaIni(new Date())
											.setNumAdulto(adultos)
											.setNumEst(estudiantes)
											.setNumInsen(insen)
											.setNumMto(maestros)
											.setNumNino(ninos)
											.setOrigen(origenValues.getValorSeleccionado());
		
		new WSClient(FirstActivity.this)
		.hasLoader(true)
		.setCancellable(false)
		.setListener(new CorridasGetListener())
		.setUrl(Url.CORRIDAS_GET)
		.execute(request.toString());
		
	}
	
	private class OrigenListener implements WSResponseListener {

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			origenes = catalog;
				
			if (null == origenValues) {
			
				origenValues = new SpinnerValues(FirstActivity.this, origenSpinner, origenes);
				origenValues.setSpinListener(new SpinnerValuesListener() {
					
					@Override
					public void ItemSelected(String texto, String value) {
						// TODO Auto-generated method stub
						Log.i("OrigenListener", texto + ": " + value);
						
						BasicNameValuePair request = new BasicNameValuePair(texto, value);
						
						new WSClient(FirstActivity.this)
						.hasLoader(false)
						.setCancellable(false)
						.setCatalogo(Catalogos.DESTINO)
						.setCifrado(Cifrados.HARD)
						.setListener(new DestinoListener())
						.setUrl(Url.DESTINO_GET)
						.execute(new DestinoReq().setOrigen(request).toString());
					}
					
					
				});
			}
		
			new WSClient(FirstActivity.this)
				.hasLoader(false)
				.setCancellable(false)
				.setCatalogo(Catalogos.DESTINO)
				.setCifrado(Cifrados.HARD)
				.setListener(new DestinoListener())
				.setUrl(Url.DESTINO_GET)
				.execute(new DestinoReq().setOrigen(origenes.get(0)).toString());
			
		}
		
	}
	
	private class DestinoListener implements WSResponseListener {

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			destinos = catalog;
			
			if(null == destinoValues)
				destinoValues = new SpinnerValues(FirstActivity.this, destinoSpinner, destinos);
			else
				destinoValues.setDatos(destinos);
		}
	}
	
	private class CorridasGetListener implements WSResponseListener {
		
		private static final String TAG  = "CorridasGetListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			Log.d(TAG, response.toString());
			
			JSONArray array = response.optJSONArray("listaCorridas");
			
			if (null != array && 0 < array.length()) {
				
				ArrayList<Corrida> corridas = new ArrayList<Corrida>(array.length());
				
				for (int i = 0; i < array.length(); i++) {
					corridas.add(new Corrida(array.optJSONObject(i)));
				}
				
				ProcessSingleton.getInstance().setAdultos(adultos);
				ProcessSingleton.getInstance().setNinos(ninos);
				ProcessSingleton.getInstance().setInsen(insen);
				ProcessSingleton.getInstance().setEstudiantes(estudiantes);
				ProcessSingleton.getInstance().setMaestros(maestros);
				ProcessSingleton.getInstance().setRedondo(TipoViaje.SENCILLO);
				ProcessSingleton.getInstance().setOrigen(origenValues.getTextoSeleccionado());
				ProcessSingleton.getInstance().setOrigenClave(origenValues.getValorSeleccionado());
				ProcessSingleton.getInstance().setDestino(destinoValues.getTextoSeleccionado());
				ProcessSingleton.getInstance().setDestinoClave(destinoValues.getValorSeleccionado());
				
				
				Intent intent = new Intent(FirstActivity.this, SecondActivity.class)
				.putParcelableArrayListExtra("corridas", corridas);
				
				startActivity(intent);
				
			} else {
				Toast.makeText(FirstActivity.this, ErrorUtil.emptyArray("corridas"), Toast.LENGTH_SHORT)
				.show();
			}
		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			
		}
		
		
		
	}
	
}
