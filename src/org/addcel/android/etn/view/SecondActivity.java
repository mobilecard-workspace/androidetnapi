package org.addcel.android.etn.view;

import java.util.ArrayList;
import java.util.List;

import org.addcel.android.crypto.Cifrados;
import org.addcel.android.etn.R;
import org.addcel.android.etn.constant.Url;
import org.addcel.android.etn.to.Asiento;
import org.addcel.android.etn.to.Corrida;
import org.addcel.android.etn.to.Diagrama;
import org.addcel.android.etn.to.DiagramaReq;
import org.addcel.android.etn.to.Itinerario;
import org.addcel.android.etn.to.ItinerarioReq;
import org.addcel.android.etn.to.OcupacionReq;
import org.addcel.android.etn.to.ProcessSingleton;
import org.addcel.android.util.ErrorUtil;
import org.addcel.android.util.Text;
import org.addcel.android.ws.client.ETNClient;
import org.addcel.android.ws.client.WSClient;
import org.addcel.android.ws.listener.WSResponseListener;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;

public class SecondActivity extends SherlockActivity {

	List<Corrida> corridas;
	Diagrama diagrama;
	Corrida c;
	List<Itinerario> itinerario;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);
		
		corridas = getIntent().getParcelableArrayListExtra("corridas");
		Log.i("SecondActivity", corridas.toString());

		c = corridas.get(0);
		((TextView) findViewById(R.id.view_corrida)).setText(c.toString());
		
		ProcessSingleton.getInstance().setCorridaSalida(c);

		Log.d("SecondActivity", ProcessSingleton.getInstance()
				.getCorridaSalida().toString());

		new WSClient(SecondActivity.this)
			.hasLoader(true)
			.setCancellable(false)
			.setCifrado(Cifrados.HARD)
			.setListener(new DiagramaGetListener())
			.setUrl(Url.DIAGRAMA_GET)
			.execute(new DiagramaReq(c).toString());

	}
	
	public void getItinerario(View v) {
		
		ItinerarioReq req = new ItinerarioReq().setCorrida(c)
												.setOrigen(ProcessSingleton.getInstance().getOrigenClave());
		
		new WSClient(SecondActivity.this)
			.hasLoader(true)
			.setCancellable(false)
			.setCifrado(Cifrados.HARD)
			.setListener(new ItinerarioGetListener())
			.setUrl(Url.ITINERARIO_GET)
			.execute(req.toString());
		
	}
	
	private class ItinerarioGetListener implements WSResponseListener {
		
		private static final String TAG = "ItinerarioGetListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			Log.e(TAG, response);
			Toast.makeText(SecondActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			
			/*
			 * "idError": 0,
				"mensajeError": "�xito",
				"listaItinerarios"

			 */
			
			int idError = response.optInt("idError");
			String mensajeError = response.optString("mensajeError", ErrorUtil.connection());

			JSONArray array = response.optJSONArray("listaItinerarios");
			
			if(array != null && array.length() > 0) {
				
				itinerario = new ArrayList<Itinerario>(array.length());
				
				String iString = "";
				
				for (int i = 0; i < array.length(); i++) {
					itinerario.add(new Itinerario(array.optJSONObject(i)));
				}
				
				for (Itinerario it : itinerario) {
					iString += it.toString();
				}
				
				AlertDialog.Builder builder = new AlertDialog.Builder(SecondActivity.this)
				.setTitle("Inicio de sesi�n MobileCard")
				.setMessage(iString)
				.setPositiveButton("OK", new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});
				
				builder.show();
				

			} else {
				mensajeError = ErrorUtil.emptyArray("itinerario");
				Toast.makeText(SecondActivity.this, mensajeError, Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			Log.e(TAG, response.toString());
			Toast.makeText(SecondActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			Log.e(TAG, "No existe cat�logo");
			Toast.makeText(SecondActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}
		
		
		
	}

	private class DiagramaGetListener implements WSResponseListener {

		private static final String TAG = "DiagramaGetListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			Log.e(TAG, response);
			Toast.makeText(SecondActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();

		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response.toString());
			
			int numError = response.optInt("numError", 10000);
			String descError = response.optString("descError", ErrorUtil.connection());
			
			switch (numError) {
			case 1111:

				diagrama = new Diagrama(response);
				
				OcupacionReq request = new OcupacionReq()
											.setCorrida(c.getCorrida())
											.setDestino(c.getDestino())
											.setOrigen(ProcessSingleton.getInstance().getOrigenClave())
											.setFecha(c.getFechaInicial());
				
				new WSClient(SecondActivity.this)
					.hasLoader(true)
					.setCancellable(false)
					.setListener(new OcupacionGetListener())
					.setUrl(Url.OCUPACION_GET)
					.execute(request.toString());
				
				break;

			default:
				Toast.makeText(SecondActivity.this, descError, Toast.LENGTH_SHORT).show();
				break;
			}
		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			Toast.makeText(SecondActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();

		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			Toast.makeText(SecondActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();

		}

	}
	
	private class OcupacionGetListener implements WSResponseListener {
		
		private static final String TAG = "OcupacionGetListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			Log.e(TAG, response);
			Toast.makeText(SecondActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response.toString());
			
			String codigoError = response.optString("codigoError", "10000");
			String descripcionError = response.optString("descripcionError", ErrorUtil.connection());
			
			if (TextUtils.equals(codigoError, "00")) {
				
				JSONArray array = response.optJSONArray("asientos");
				
				if (null != array && 0 < array.length()) {
					
					if (0 < diagrama.getNumFilasArriba()) {
						
						
					} else {
						
						int asientoIndex = 0;
						
						for (Asiento a : diagrama.getDiagramaAutobus()) {
							
							
							
							if(a.isEsAsiento()) {
								a.setOcupado(array.optInt(asientoIndex));
								asientoIndex++;
							}
						}
						
						Log.i(TAG, diagrama.getDiagramaAutobus().toString());
						
						
					}
					
				} else {
					Toast.makeText(SecondActivity.this, ErrorUtil.emptyArray("asientos"), Toast.LENGTH_SHORT).show();
				}
				
				
			} else {
				Toast.makeText(SecondActivity.this, descripcionError, Toast.LENGTH_SHORT).show();
			}
			
			
//			{
//				"codigoError":"00",
//				"asientos":["0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","1","0","0","0","0","0","0","0","0"],
//				"descripcionError":"Sin Error."
//			}

			
		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			Log.e(TAG, response.toString());
			Toast.makeText(SecondActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			Log.e(TAG, "No existe catalogo");
			Toast.makeText(SecondActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}
		
		
		
	}

}
