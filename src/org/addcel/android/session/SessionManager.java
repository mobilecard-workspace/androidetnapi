package org.addcel.android.session;

import java.util.HashMap;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionManager {
	SharedPreferences pref;
	Editor editor;
	Context _context;
	
	int PRIVATE_MODE = 0;
	
	private static final String PREF_NAME = "mobilecard";
	private static final String IS_LOGIN = "IsLoggedIn";
	public static final String USR_ID = "usr_id";
	public static final String USR_LOGIN = "usr_login";
	public static final String USR_PWD = "usr_pwd";
	public static final String PUSH_ID = "push_id";
	
	public SessionManager(Context context) {
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}
	
	public void createLoginSession(String id, String login, String pwd, String pushId) {
		editor.putBoolean(IS_LOGIN, true);
		editor.putString(USR_ID, id);
		editor.putString(USR_LOGIN, login);
		editor.putString(USR_PWD, pwd);
		editor.putString(PUSH_ID, pushId);
		editor.commit();
	}
	
	public HashMap<String, String> getUserDetails() {
		HashMap<String, String> user = new HashMap<String, String>();
		
		user.put(USR_ID, pref.getString(USR_ID, null));
		user.put(USR_LOGIN, pref.getString(USR_LOGIN, null));
		user.put(USR_PWD, pref.getString(USR_PWD, null));
		user.put(PUSH_ID, pref.getString(PUSH_ID, null));
		
		return user;
	}
	
//	public void checkLogin() {
//		if (!this.isLoggedIn()) {
//            Intent i = new Intent(_context, org.addcel.edomex.view.LoginActivity.class);
//            // Closing all the Activities
//            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//             
//            // Add new Flag to start new Activity
//            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//             
//            // Staring Login Activity
//            _context.startActivity(i);
//		}
//	}
//	
	public void setPushId(String pushId) {
		editor.putString(PUSH_ID, pushId);
		editor.commit();
	}
	
	public void setPassword(String pwd) {
		editor.putString(USR_PWD, pwd);
		editor.commit();
	}
	
	public void logoutUser() {
		editor.clear();
		editor.commit();
	}
	
	public boolean isLoggedIn() {
		return pref.getBoolean(IS_LOGIN, false);
	}
	
}
