package org.addcel.android.font;

public enum Fonts {
	ROBOTO_BLACK("Black"), 
	ROBOTO_BLACKITALIC("BlackItalic"),
	ROBOTO_BOLD("Bold"),
	ROBOTO_BOLDITALIC("BoldItalic"),
	ROBOTO_ITALIC("Italic"),
	ROBOTO_LIGHT("Light"),
	ROBOTO_LIGHTITALIC("LightItalic"),
	ROBOTO_MEDIUM("Medium"),
	ROBOTO_MEDIUMITALIC("MediumItalic"),
	ROBOTO_REGULAR("Regular"),
	ROBOTO_THIN("Thin"),
	ROBOTO_THINITALIC("ThinItalic");
	
	private String path;
	
	private Fonts(String _p) {
		path = "fonts/Roboto/Roboto-" + _p + ".ttf";
	}
	
	public String getPath() {
		return path;
	}
}
