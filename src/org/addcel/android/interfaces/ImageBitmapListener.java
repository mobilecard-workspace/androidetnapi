package org.addcel.android.interfaces;

import android.graphics.Bitmap;

public interface ImageBitmapListener {
	public void imageLoaded(Bitmap bmp, int pos);
}
