package org.addcel.android.interfaces;

import android.widget.ImageView;

public interface ImageLoadedListener {
	public void imageLoaded(ImageView bmp, int pos);
}
