package org.addcel.android.util;

import org.addcel.android.interfaces.DialogOkListener;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

public class Dialogos {
	
	private static ProgressDialog progress;
	
	public static ProgressDialog makeDialog(Context con, String title, String msj){
		progress = new ProgressDialog(con);
		progress.setTitle(title);
		progress.setMessage(msj);
		progress.setIndeterminate(true);
		progress.show();
		return progress;
	}
	
	public static void closeDialog(){
		if(progress.isShowing()){
			progress.dismiss();
		}
	}
	
	public static void makeYesNoAlert(Context con, String msj, final DialogOkListener listener){
		AlertDialog.Builder builder = new AlertDialog.Builder(con);
		builder.setMessage(msj)
		       .setCancelable(false)
		       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		                listener.ok(dialog, id);
		           }
		       })
		       .setNegativeButton("No", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		        	   dialog.cancel();
		                listener.cancel(dialog, id);
		           }
		       });
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	public static void makeOkAlert(Context con, String msj, final DialogOkListener listener){
		AlertDialog.Builder builder = new AlertDialog.Builder(con);
		builder.setMessage(msj)
		       .setCancelable(false)
		       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		                listener.ok(dialog, id);
		           }
		       });
		
		AlertDialog alert = builder.create();
		
		alert.show();
	}
}
