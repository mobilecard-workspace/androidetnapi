package org.addcel.android.mobilecard.enums;

public enum Meses {

	ENERO(0, "01"), FEBRERO(1, "02"), MARZO(2, "03"), ABRIL(3, "04"), MAYO(4,
			"05"), JUNIO(5, "06"), JULIO(6, "07"), AGOSTO(7, "08"), SEPTIEMBRE(
			8, "09"), OCTUBRE(9, "10"), NOVIEMBRE(10, "11"), DICIEMBRE(11, "12");

	private int index;
	private String clave;

	private Meses(int _index, String _clave) {
		index = _index;
		clave = _clave;
	}

	public String getClave() {
		return clave;
	}
	
	public int getIndex() {
		return index;
	}

	public static Meses pickMes(String _clave) {

		if (_clave.equals(ENERO.getClave()))
			return ENERO;
		else if (_clave.equals(FEBRERO.getClave()))
			return FEBRERO;
		else if (_clave.equals(MARZO.getClave()))
			return MARZO;
		else if (_clave.equals(ABRIL.getClave()))
			return ABRIL;
		else if (_clave.equals(MAYO.getClave()))
			return MAYO;
		else if (_clave.equals(JUNIO.getClave()))
			return JUNIO;
		else if (_clave.equals(JULIO.getClave()))
			return JULIO;
		else if (_clave.equals(AGOSTO.getClave()))
			return AGOSTO;
		else if (_clave.equals(SEPTIEMBRE.getClave()))
			return SEPTIEMBRE;
		else if (_clave.equals(OCTUBRE.getClave()))
			return OCTUBRE;
		else if (_clave.equals(NOVIEMBRE.getClave()))
			return NOVIEMBRE;
		else if (_clave.equals(DICIEMBRE.getClave()))
			return DICIEMBRE;
		else
			return null;

	}

}
