package org.addcel.android.mobilecard.constant;

public class Url {

	private static final String DEV = "http://50.57.192.214:8080/";
	private static final String PROD = "http://www.mobilecard.mx:8080/";

	private static final String CONTEXT = DEV;

	private final static String BASE = CONTEXT + "AddCelBridge/Servicios/";
	
	public final static String LOGIN = BASE + "adc_userLogin.jsp";
	public final static String INSERT = BASE + "adc_userInsert.jsp";
	public final static String UPDATE = BASE + "adc_userUpdate.jsp";
	public final static String UPDATE_PASS = BASE + "adc_userPasswordUpdate.jsp";
	public final static String USER_GET = BASE + "adc_getUserData.jsp";
	public final static String CARD_TYPE_GET = BASE + "adc_getCardType.jsp";
	public final static String PROVIDERS_GET = BASE + "adc_getProviders.jsp";
	public final static String TERMINOS = BASE + "adc_getConditions.jsp";
	

	public final static String ESTADOS_GET = BASE + "adc_getEstados.jsp";
	
	public final static String COMISION_GET = BASE + "adc_getComision.jsp";
	public final static String RECOVERY_GET = BASE + "adc_RecoveryUP.jsp";
	public static final String USER_PASS_MAIL = BASE + "adc_userPasswordUpdateMail.jsp";
	
	public static final String SET_SMS = BASE + "adc_setSMS.jsp";

	public final static String INVITA = CONTEXT + "AddCelBridge/invita";

}
