package org.addcel.android.mobilecard.to;

import org.addcel.android.interfaces.JSONable;
import org.addcel.android.mobilecard.enums.Sexo;
import org.addcel.android.util.AppUtils;
import org.addcel.android.util.DeviceUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class Usuario implements JSONable, Parcelable {
	
	private String apellido;
	private long usuario;
	private String materno;
	private String vigencia;
	private String telOficina;
	private Sexo sexo;
	private int numExt;
	private String password;
	private String colonia;
	private String nombre;
	private String login;
	private int tipoCliente;
	private int proveedor;
	private int idEstado;
	private String mail;
	private String ciudad;
	private String nacimiento;
	private String telefono;
	private int tipoTarjeta;
	private String tarjeta;
	private String maskedTarjeta;
	private String telCasa;
	private String cp;
	private String registro;
	private String numInterior;
	private String calle;
	
	public Usuario() {}
	
	public Usuario(Parcel source) {
		readFromParcel(source);
	}
	
	public Usuario(JSONObject json) {
		fromJSON(json);
	}
	
	public String getApellido() {
		return apellido;
	}

	public long getUsuario() {
		return usuario;
	}

	public String getMaterno() {
		return materno;
	}

	public String getVigencia() {
		return vigencia;
	}

	public String getTelOficina() {
		return telOficina;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public int getNumExt() {
		return numExt;
	}

	public String getPassword() {
		return password;
	}

	public String getColonia() {
		return colonia;
	}

	public String getNombre() {
		return nombre;
	}

	public String getLogin() {
		return login;
	}

	public int getTipoCliente() {
		return tipoCliente;
	}

	public int getProveedor() {
		return proveedor;
	}

	public int getIdEstado() {
		return idEstado;
	}

	public String getMail() {
		return mail;
	}

	public String getCiudad() {
		return ciudad;
	}

	public String getNacimiento() {
		return nacimiento;
	}

	public String getTelefono() {
		return telefono;
	}

	public int getTipoTarjeta() {
		return tipoTarjeta;
	}

	public String getTarjeta() {
		return tarjeta;
	}
	
	public String getMaskedTarjeta() {
		return maskedTarjeta;
	}

	public String getTelCasa() {
		return telCasa;
	}

	public String getCp() {
		return cp;
	}

	public String getRegistro() {
		return registro;
	}

	public String getNumInterior() {
		return numInterior;
	}

	public String getCalle() {
		return calle;
	}

	public Usuario setApellido(String apellido) {
		this.apellido = apellido;
		return this;
	}

	public Usuario setUsuario(long usuario) {
		this.usuario = usuario;
		return this;
	}

	public Usuario setMaterno(String materno) {
		this.materno = materno;
		return this;
	}

	public Usuario setVigencia(String vigencia) {
		this.vigencia = vigencia;
		return this;
	}

	public Usuario setTelOficina(String telOficina) {
		this.telOficina = telOficina;
		return this;
	}

	public Usuario setSexo(Sexo sexo) {
		this.sexo = sexo;
		return this;
	}

	public Usuario setNumExt(int numExt) {
		this.numExt = numExt;
		return this;
	}

	public Usuario setPassword(String password) {
		this.password = password;
		return this;
	}

	public Usuario setColonia(String colonia) {
		this.colonia = colonia;
		return this;
	}

	public Usuario setNombre(String nombre) {
		this.nombre = nombre;
		return this;
	}

	public Usuario setLogin(String login) {
		this.login = login;
		return this;
	}

	public Usuario setTipoCliente(int tipoCliente) {
		this.tipoCliente = tipoCliente;
		return this;
	}

	public Usuario setProveedor(int proveedor) {
		this.proveedor = proveedor;
		return this;
	}

	public Usuario setIdEstado(int idEstado) {
		this.idEstado = idEstado;
		return this;
	}

	public Usuario setMail(String mail) {
		this.mail = mail;
		return this;
	}

	public Usuario setCiudad(String ciudad) {
		this.ciudad = ciudad;
		return this;
	}

	public Usuario setNacimiento(String nacimiento) {
		this.nacimiento = nacimiento;
		return this;
	}

	public Usuario setTelefono(String telefono) {
		this.telefono = telefono;
		return this;
	}

	public Usuario setTipoTarjeta(int tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
		return this;
	}

	public Usuario setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
		return this;
	}
	
	public Usuario setMaskedTarjeta(String maskedTarjeta) {
		this.maskedTarjeta = maskedTarjeta;
		return this;
	}

	public Usuario setTelCasa(String telCasa) {
		this.telCasa = telCasa;
		return this;
	}

	public Usuario setCp(String cp) {
		this.cp = cp;
		return this;
	}

	public Usuario setRegistro(String registro) {
		this.registro = registro;
		return this;
	}

	public Usuario setNumInterior(String numInterior) {
		this.numInterior = numInterior;
		return this;
	}

	public Usuario setCalle(String calle) {
		this.calle = calle;
		return this;
	}
	
	private String maskTarjeta() {
		
		maskedTarjeta = "";
		
		for (int i = 0; i < tarjeta.length(); i++) {
			
			if ( i < 12)
				maskedTarjeta += "*";
			else
				maskedTarjeta += tarjeta.charAt(i);
		}
		
		return maskedTarjeta;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Login -> " + login + ", Id -> " + usuario;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(apellido);
		dest.writeLong(usuario);
		dest.writeString(materno);
		dest.writeString(vigencia);
		dest.writeString(telOficina);
		dest.writeSerializable(sexo);
		dest.writeInt(numExt);
		dest.writeString(password);
		dest.writeString(colonia);
		dest.writeString(nombre);
		dest.writeString(login);
		dest.writeInt(tipoCliente);
		dest.writeInt(proveedor);
		dest.writeInt(idEstado);
		dest.writeString(mail);
		dest.writeString(ciudad);
		dest.writeString(nacimiento);
		dest.writeString(telefono);
		dest.writeInt(tipoTarjeta);
		dest.writeString(tarjeta);
		dest.writeString(maskedTarjeta);
		dest.writeString(telCasa);
		dest.writeString(cp);
		dest.writeString(registro);
		dest.writeString(numInterior);
		dest.writeString(calle);
	}
	
	public void readFromParcel(Parcel source) {
		apellido = source.readString();
		usuario = source.readLong();
		materno = source.readString();
		vigencia = source.readString();
		telOficina = source.readString();
		sexo = (Sexo) source.readSerializable();
		numExt = source.readInt();
		password = source.readString();
		colonia = source.readString();
		nombre = source.readString();
		login = source.readString();
		tipoCliente = source.readInt();
		proveedor = source.readInt();
		idEstado = source.readInt();
		mail = source.readString();
		ciudad = source.readString();
		nacimiento = source.readString();
		telefono = source.readString();
		tipoTarjeta = source.readInt();
		tarjeta = source.readString();
		maskedTarjeta = source.readString();
		telCasa = source.readString();
		cp = source.readString();
		registro = source.readString();
		numInterior = source.readString();
		calle = source.readString();
	}

	@Override
	public JSONObject toJSON() {
		// TODO Auto-generated method stub
		
		try {
			
			return new JSONObject()
				.put("apellido", apellido)
				.put("usuario", usuario)
				.put("materno", materno)
				.put("vigencia", vigencia)
				.put("tel_oficina", telOficina)
				.put("sexo", sexo.getClave())
				.put("num_ext", numExt)
				.put("password", password)
				.put("colonia", colonia)
				.put("nombre", nombre)
				.put("login", login)
				.put("tipo_cliente", tipoCliente)
				.put("proveedor", proveedor)
				.put("id_estado", idEstado)
				.put("mail", mail)
				.put("banco", 1)
				.put("ciudad", ciudad)
				.put("status", 1)
				.put("nacimiento", nacimiento)
				.put("telefono", telefono)
				.put("tipotarjeta", tipoTarjeta)
				.put("tarjeta", tarjeta)
				.put("tel_casa", telCasa)
				.put("cp", cp)
				.put("registro", registro)
				.put("num_interior", numInterior)
				.put("calle", calle);
			
		} catch (JSONException e) {
			return null;
		}
	}

	@Override
	public void fromJSON(JSONObject json) {
		// TODO Auto-generated method stub
		apellido = json.optString("apellido");
		usuario = json.optLong("usuario");
		materno = json.optString("materno");
		vigencia = json.optString("vigencia");
		telOficina = json.optString("tel_oficina");
		
		if (json.optString("sexo").equals(Sexo.MASCULINO.getClave()))
			sexo = Sexo.MASCULINO;
		
		if (json.optString("sexo").equals(Sexo.FEMENINO.getClave()))
			sexo = Sexo.FEMENINO;
		
		numExt = json.optInt("num_ext");
		password = json.optString("password");
		colonia = json.optString("colonia");
		nombre = json.optString("nombre");
		login = json.optString("login");
		tipoCliente = json.optInt("tipo_cliente");
		proveedor = json.optInt("proveedor");
		idEstado = json.optInt("id_estado");
		mail = json.optString("mail");
		ciudad = json.optString("ciudad");
		nacimiento = json.optString("nacimiento");
		telefono = json.optString("telefono");
		tipoTarjeta = json.optInt("tipotarjeta");
		tarjeta = json.optString("tarjeta");
		maskedTarjeta = maskTarjeta();
		telCasa = json.optString("tel_casa");
		cp = json.optString("cp");
		registro = json.optString("registro");
		numInterior = json.optString("num_interior");
		calle = json.optString("calle");
	}
	
	public String buildRegistroRequest (Context _con) {
		
		try {
			
			return toJSON().put("idtiporecargatag", 0)
							.put("etiqueta", "")
							.put("numero", "")
							.put("dv", 0)
							.put("imei", DeviceUtils.getIMEI(_con))
							.put("tipo", DeviceUtils.getTipo())
							.put("software", DeviceUtils.getSWVersion())
							.put("modelo", DeviceUtils.getModel())
							.put("key", DeviceUtils.getIMEI(_con))
							.toString();
			
		} catch (JSONException e) {
			
			return null;
		}
		
	}
	
	public static final Parcelable.Creator<Usuario> CREATOR = new Parcelable.Creator<Usuario>() {

		@Override
		public Usuario createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new Usuario(source);
		}

		@Override
		public Usuario[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Usuario[size];
		}
	};

}
