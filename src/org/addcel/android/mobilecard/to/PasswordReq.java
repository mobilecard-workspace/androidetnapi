package org.addcel.android.mobilecard.to;

import org.addcel.android.crypto.Crypto;
import org.addcel.android.interfaces.JSONable;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class PasswordReq implements JSONable, Parcelable {
	
	private int status;
	private long id;
	private String login;
	private String passwordS;
	private String password;
	
	public PasswordReq() {}
	
	public PasswordReq(Parcel source) {
		readFromParcel(source);
	}
	
	public PasswordReq(JSONObject json) {
		fromJSON(json);
	}
	
	public int getStatus() {
		return status;
	}
	
	public long getId() {
		return id;
	}
	
	public String getLogin() {
		return login;
	}
	
	public String getPasswordS() {
		return passwordS;
	}
	
	public String getPassword() {
		return password;
	}
	
	public PasswordReq setStatus(int _status) {
		status = _status;
		
		return this;
	}
	
	public PasswordReq setId(long _id) {
		id = _id;
		
		return this;
	}
	
	public PasswordReq setLogin(String _login) {
		login = _login;
		
		return this;
	}
	
	public PasswordReq setPasswordS(String _passwordS) {
		passwordS = _passwordS;
		
		return this;
	}
	
	public PasswordReq setPassword(String _password) {
		password = _password;
		
		return this;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(status);
		dest.writeLong(id);
		dest.writeString(login);
		dest.writeString(passwordS);
		dest.writeString(password);
	}
	
	public void readFromParcel(Parcel source) {
		status = source.readInt();
		id = source.readLong();
		login = source.readString();
		passwordS = source.readString();
		password = source.readString();
	}

	@Override
	public JSONObject toJSON() {
		// TODO Auto-generated method stub
		
		JSONObject json = new JSONObject();
		
		try {
			
			json.put("login", login).put("newPassword", password);
			
			switch (status) {
			case 99:
				
				return json.put("passwordS", Crypto.sha1(passwordS))
						.put("password", password);
			
			default:
				
				return json.put("password", passwordS);
				
			}
			
		} catch (JSONException e) {

			return null;
		}
		
	}

	@Override
	public void fromJSON(JSONObject json) {
		// TODO Auto-generated method stub

	}
	
	public static final Parcelable.Creator<PasswordReq> CREATOR = new Creator<PasswordReq>() {
		
		@Override
		public PasswordReq[] newArray(int size) {
			// TODO Auto-generated method stub
			return new PasswordReq[size];
		}
		
		@Override
		public PasswordReq createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new PasswordReq(source);
		}
	};

}
